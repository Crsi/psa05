#!/usr/bin/env python3

import time

from common import BaseTest, main
from config import PASSWD_MYSQL_FOO, PASSWD_MYSQL_BAR, PASSWD_MYSQL_REPLICATION, PASSWD_MYSQL_SLAVE_BACKUP


class Tests05(BaseTest):
    def test_master_mariadb_server_running(self):
        for line in self.execute("service --status-all").stdout.decode("UTF-8").split("\n"):
            if "mariadb" in line:
                self.assertIn("+", line)
        self.systemd("mariadb.service")

    def test_slave_mariadb_server_running(self):
        for line in self.execute("ssh 192.168.5.1 service --status-all").stdout.decode("UTF-8").split("\n"):
            if "mariadb" in line:
                self.assertIn("+", line)
        self.systemd("mariadb.service", ip="192.168.5.1")

    def test_mariadb_foo_login_from_localhost(self):
        stdout = self.execute(
            f"mysql -u foo -p{PASSWD_MYSQL_FOO} -h 127.0.0.1 -e 'SHOW TABLES;' foo"
        ).stdout.decode("UTF-8")
        self.assertIn("Tables_in_foo\nfoo", stdout)

    def test_mariadb_foo_no_login_from_node01(self):
        stderr = self.execute(
            f"ssh 192.168.5.1 \"mysql -u foo -p{PASSWD_MYSQL_FOO} -h 192.168.5.4 -e 'SHOW TABLES;' foo\"", 1
        ).stderr.decode("UTF-8")
        self.assertIn("Access denied for user 'foo'@'192.168.5.1'", stderr)

    def test_mariadb_bar_login_from_node01(self):
        stdout = self.execute(
            f"ssh 192.168.5.1 \"mysql -u bar -p{PASSWD_MYSQL_BAR} -h 192.168.5.4 -e 'SHOW TABLES;' bar\""
        ).stdout.decode("UTF-8")
        self.assertIn("Tables_in_bar\nbar", stdout)

    def test_mariadb_bar_no_login_from_localhost(self):
        stderr = self.execute(
            f"mysql -u bar -p{PASSWD_MYSQL_BAR} -e 'SHOW TABLES;' bar", 1
        ).stderr.decode("UTF-8")
        self.assertIn("Access denied for user 'bar'@'localhost'", stderr)

    def test_mariadb_replication_logins_and_databases(self):
        def check(host) -> str:
            return self.execute(
                f"mysql -u replication -p{PASSWD_MYSQL_REPLICATION} {host} -e 'SHOW DATABASES;'"
            ).stdout.decode("UTF-8")

        stdout_localhost = check("")
        self.assertIn("information_schema", check(""))
        self.assertNotIn("bar", stdout_localhost)
        self.assertNotIn("foo", stdout_localhost)
        self.assertNotIn("mysql", stdout_localhost)
        self.assertNotIn("performance_schema", stdout_localhost)

        stdout_node01 = check("-h 192.168.5.4")
        self.assertIn("information_schema", stdout_node01)
        self.assertNotIn("bar", stdout_node01)
        self.assertNotIn("foo", stdout_node01)
        self.assertNotIn("mysql", stdout_node01)
        self.assertNotIn("performance_schema", stdout_node01)

    def test_mariadb_slave_backup_login(self):
        stdout = self.execute(
            f"ssh 192.168.5.1 \"mysql -u _slave_backup -p{PASSWD_MYSQL_SLAVE_BACKUP} -e 'SHOW DATABASES;'\""
        ).stdout.decode("UTF-8")
        self.assertIn("bar", stdout)
        self.assertIn("foo", stdout)
        self.assertIn("information_schema", stdout)
        self.assertIn("mysql", stdout)
        self.assertIn("performance_schema", stdout)

    def test_mariadb_backup_script_exists(self):
        stdout = self.execute("ssh 192.168.5.1 ls /root/backup-mariadb.sh").stdout.decode("UTF-8")
        self.assertIn("/root/backup-mariadb.sh", stdout)

    def test_mariadb_recent_backup(self):
        stdout = self.execute("ssh 192.168.5.1 ls /var/backups/mariadb/").stdout.decode("UTF-8")
        date = time.strftime("%Y-%m-%d", time.localtime())
        self.assertIn(f"dump-{date}_00-00.sql.gz", stdout)


if __name__ == "__main__":
    main()
