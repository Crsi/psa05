#!/usr/bin/env python3

import os

import urllib3
import requests

from common import BaseTest, main


class Tests04(BaseTest):
    def test_nginx_running(self):
        self.systemd("nginx.service")

    def test_nginx_ports(self):
        self.nmap("127.0.0.1", [(80, "tcp", "open"), (443, "tcp", "open")])
        self.nmap("192.168.5.3", [(80, "tcp", "open"), (443, "tcp", "open")])
        self.nmap("192.168.5.4", [(80, "tcp", "open"), (443, "tcp", "open")])

    def test_http_https_localhost_connections(self):
        urllib3.disable_warnings()
        self.assertEqual(200, requests.get("http://127.0.0.1/", verify=False).status_code)
        self.assertEqual(200, requests.get("https://127.0.0.1/", verify=False).status_code)
        self.assertEqual(200, requests.get("http://192.168.5.3/", verify=False).status_code)
        self.assertEqual(200, requests.get("https://192.168.5.3/", verify=False).status_code)
        self.assertEqual(200, requests.get("http://192.168.5.4/", verify=False).status_code)
        self.assertEqual(200, requests.get("https://192.168.5.4/", verify=False).status_code)

    def test_http_https_all_websites(self):
        urllib3.disable_warnings()

        page1_http = requests.get("http://services.psa-team05.in.tum.de/", verify=False)
        page1_https = requests.get("https://services.psa-team05.in.tum.de/", verify=False)
        self.assertEqual(200, page1_https.status_code)
        self.assertEqual(page1_http.content, page1_https.content)

        page2_http = requests.get("http://unknown.psa-team05.in.tum.de/", verify=False)
        page2_https = requests.get("https://unknown.psa-team05.in.tum.de/", verify=False)
        self.assertEqual(200, page2_https.status_code)
        self.assertEqual(page2_http.content, page2_https.content)

        page3_http = requests.get("http://gitea.psa-team05.in.tum.de/", verify=False)
        page3_https = requests.get("https://gitea.psa-team05.in.tum.de/", verify=False)
        self.assertEqual(200, page3_https.status_code)
        self.assertGreaterEqual(len(page3_http.history), 1)

        page4_http = requests.get("http://main.psa-team05.in.tum.de/", verify=False)
        page4_https = requests.get("https://main.psa-team05.in.tum.de/", verify=False)
        self.assertEqual(200, page4_https.status_code)
        self.assertEqual(page4_http.content, page4_https.content)

        self.assertNotEqual(page1_http.content, page2_http.content)
        self.assertNotEqual(page1_http.content, page3_http.content)
        self.assertNotEqual(page1_http.content, page4_http.content)

    def test_http_https_home_dirs_static(self):
        self.assertTrue(os.path.exists("/home/ge96raf"))
        self.assertTrue(os.path.exists("/home/ge96raf/.html-data"))
        self.assertIn(
            requests.get("http://services.psa-team05.in.tum.de/~ge96raf/", verify=False).status_code,
            [200, 301, 307]
        )
        self.assertEqual(
            404,
            requests.get("http://services.psa-team05.in.tum.de/~ge96raf/nonexistent", verify=False).status_code
        )
        self.assertTrue(os.path.exists("/home/ge96raf/.html-data/some_file.html"))
        with open("/home/ge96raf/.html-data/some_file.html") as f:
            content = f.read()
        self.assertEqual(content, requests.get(
            "http://services.psa-team05.in.tum.de/~ge96raf/some_file.html", verify=False
        ).text)

    def test_http_https_home_dir_instant_updates(self):
        self.assertTrue(os.path.exists("/home/ge96raf"))
        self.assertTrue(os.path.exists("/home/ge96raf/.html-data"))

        content = self.random_string(64)
        filename = self.random_string(16)
        path = os.path.join("/home", "ge96raf", ".html-data", filename)
        with open(path, "w") as f:
            f.write(content)
        response = requests.get(f"http://services.psa-team05.in.tum.de/~ge96raf/{filename}", verify=False)
        self.assertEqual(200, response.status_code)
        self.assertEqual(content, response.text)
        os.unlink(path)

    def test_http_https_home_dirs_dynamic(self):
        self.assertTrue(os.path.exists("/home/ge96raf"))
        self.assertTrue(os.path.exists("/home/ge96raf/.cgi-bin"))
        self.assertTrue(os.path.exists("/home/ge96raf/.cgi-bin/random_int.py"))
        self.assertTrue(os.path.exists("/home/ge96raf/.cgi-bin/hello_world.pl"))

        perl_response = requests.get(
            "https://services.psa-team05.in.tum.de/~ge96raf/cgi-bin/hello_world.pl", verify=False
        )
        self.assertEqual(200, perl_response.status_code)
        self.assertIn(b"Hello World!", perl_response.content)
        self.assertIn(b"Perl", perl_response.content)

        bash_response = requests.get(
            "https://services.psa-team05.in.tum.de/~ge96raf/cgi-bin/hello_world.sh", verify=False
        )
        self.assertEqual(200, bash_response.status_code)
        self.assertIn(b"Hello World!", bash_response.content)
        self.assertIn(b"Bash", bash_response.content)

        results = sorted([
            int(requests.get(
                "http://services.psa-team05.in.tum.de/~ge96raf/cgi-bin/random_int.py", verify=False
            ).content)
            for _ in range(8)
        ])
        for k in results[:]:
            results.remove(k)
            self.assertNotIn(k, results, "same entry twice")

    def test_http_https_home_dynamic_by_user(self):
        self.assertTrue(os.path.exists("/home/ge96raf/.cgi-bin/"))
        self.assertTrue(os.path.exists("/home/ge96raf/.cgi-bin/environ.py"))

        environ = requests.get(
            "https://services.psa-team05.in.tum.de/~ge96raf/cgi-bin/environ.py", verify=False
        ).json()
        self.assertIn("USER", environ)
        self.assertEqual("ge96raf", environ["USER"], "user executing the scripts isn't correct!")

    def test_log_files_no_sensitive_data(self):
        self.assertTrue(os.path.exists("/var/log/nginx/access.log"))

        requests.get("https://127.0.0.1", verify=False)
        requests.get("https://192.168.5.3", verify=False)
        requests.get("https://192.168.5.4", verify=False)
        with open("/var/log/nginx/access.log", "r") as f:
            for line in f:
                self.assertNotIn("127.0.0.1", line)
                self.assertNotIn("192.168.5.3", line)
                self.assertNotIn("192.168.5.4", line)

    def test_log_rotation(self):
        self.systemd("logrotate.timer")
        self.assertTrue(os.path.exists("/var/log/nginx"))
        self.assertTrue(os.path.exists("/var/log/nginx/access.log"))
        self.assertTrue(os.path.exists("/var/log/nginx/error.log"))

        log_files = os.listdir("/var/log/nginx/")
        self.assertLessEqual(len([i for i in log_files if i.startswith("access.log")]), 6)
        self.assertLessEqual(len([i for i in log_files if i.startswith("error.log")]), 2)


if __name__ == "__main__":
    main()
