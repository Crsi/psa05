#!/usr/bin/env python3

import os
import socket

from common import BaseTest, main
from config import PSA_IP


class Tests01(BaseTest):
    def test_is_root(self):
        self.assertTrue(os.geteuid() == 0, "You should execute this script as root!")

    def test_dns_works(self):
        self.assertEqual(socket.gethostbyname("psarbg2.in.tum.de"), socket.gethostbyname("psa.in.tum.de"))
        self.assertTrue(socket.gethostbyname("tum.de"))

    def test_ssh_local_root_login(self):
        self.execute("ssh root@192.168.5.3 id")

    def test_ssh_to_gateway(self):
        self.execute("ssh root@192.168.5.2 id")

    def test_ssh_to_monitor(self):
        self.execute("ssh root@192.168.5.1 id")

    def test_ssh_remote_root_login(self):
        self.execute(f"ssh -J root@{PSA_IP}:60520 root@192.168.5.3 id")

    def test_disk_not_full(self):
        for node in ["192.168.5.1", "192.168.5.2", "192.168.5.3"]:
            for line in self.execute(f"ssh {node} df").stdout.decode("UTF-8").split("\n")[1:]:
                if line != "":
                    percent = int(line.split("%")[0].split(" ")[-1])
                    self.assertLessEqual(percent, 95, f"Low disk space on {node}: {line}")

    def test_memory_available(self):
        with open("/proc/meminfo", "r") as f:
            content = f.read()
        for line in content.split("\n"):
            if line.startswith("MemFree:"):
                self.assertGreaterEqual(int(line[:-3].split(" ")[-1]), 1024*4, "MemFree low!")
            elif line.startswith("MemAvailable:"):
                self.assertGreaterEqual(int(line[:-3].split(" ")[-1]), 1024*16, "MemAvailable low!")


if __name__ == "__main__":
    main()
