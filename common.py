import sys
import shlex
import random
import string
import typing
import unittest
import subprocess

from config import DEFAULT_SUBPROCESS_TIMEOUT, UNITTEST_VERBOSE_DEFAULT


def main():
    args = sys.argv[:]
    if "-v" not in args and UNITTEST_VERBOSE_DEFAULT:
        args.append("-v")
    unittest.main(argv=args)


class BaseTest(unittest.TestCase):
    @staticmethod
    def random_string(length: int, charset: str = string.ascii_lowercase):
        return "".join(random.choice(charset) for _ in range(length))

    def execute(
            self,
            cmd: typing.Union[str, typing.List[str]],
            code: typing.Optional[int] = 0,
            timeout: typing.Optional[int] = DEFAULT_SUBPROCESS_TIMEOUT
    ) -> subprocess.CompletedProcess:
        """Execute arbitrary commands and optionally compare their return code"""
        try:
            if isinstance(cmd, str):
                p = subprocess.run(shlex.split(cmd), capture_output=True, timeout=timeout)
            elif isinstance(cmd, list):
                p = subprocess.run(cmd, capture_output=True, timeout=timeout)
            else:
                raise TypeError(f"{cmd!r} is not of type str or list of str")
        except subprocess.TimeoutExpired as exc:
            self.fail(f"Timeout expired for {cmd!r}: {exc!s}")
        if code is not None:
            self.assertEqual(p.returncode, code, f"Failing for {cmd!r}")
        return p

    def nmap(self, host: str, ports: typing.List[typing.Tuple[int, str, str]], fail: bool = True) -> bool:
        fails = []
        for port in ports:
            stdout = self.execute(f"nmap {host} -p {port[0]}", timeout=None).stdout.decode("UTF-8")
            if not any(f"{port[0]}/{port[1]} {port[2]}" in line for line in stdout.split("\n")):
                fails.append(str(port[0]))
        if not fails:
            return True
        if fail:
            self.fail(f"nmap failed for host {host}, ports: " + " ,".join(fails))
        return False

    def dig(self, query: str, options: str = "", nameserver: typing.Optional[str] = None, strip: bool = True) -> str:
        stdout = self.execute(
            f"dig {('@'+nameserver) if nameserver else ''} {options} {query}",
            timeout=None
        ).stdout.decode("UTF-8")
        if strip:
            return "\n".join([line for line in stdout.split("\n") if not line.startswith(";") and len(line) > 1])
        return stdout

    def systemd(self, unit_name: str, active: bool = True, enabled: bool = True, ip: str = None) -> bool:
        is_active = is_enabled = True
        if active:
            if ip:
                is_active = self.execute(f"ssh root@{ip} -- systemctl is-active {unit_name}").returncode == 0
            else:
                is_active = self.execute(f"systemctl is-active {unit_name}").returncode == 0
        if enabled:
            if ip:
                is_enabled = self.execute(f"ssh root@{ip} -- systemctl is-enabled {unit_name}").returncode == 0
            else:
                is_enabled = self.execute(f"systemctl is-enabled {unit_name}").returncode == 0
        return is_active and is_enabled
