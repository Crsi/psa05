"""
Python module to hold the config settings that may be overwritten by clients
"""

UNITTEST_VERBOSE_DEFAULT = True
DEFAULT_SUBPROCESS_TIMEOUT = 3  # seconds

OUR_DOMAIN = "psa-team05.in.tum.de"

ALL_GROUP_ROUTERS = [(1, 1), (2, 3), (3, 1), (4, 3), (5, 2), (6, 3), (7, 2), (8, 2), (9, 1)]

GATEWAY_IPS = [
    "192.168.5.2",
    "192.168.51.2",
    "192.168.52.2",
    "192.168.53.2",
    "192.168.54.2",
    "192.168.65.1",
    "192.168.75.1",
    "192.168.85.1",
    "192.168.95.1",

]

REMOTE_ROUTER_IPS = [
    "192.168.51.1",
    "192.168.52.1",
    "192.168.53.1",
    "192.168.54.1",
    "192.168.65.2",
    "192.168.75.2",
    "192.168.85.2",
    "192.168.95.2",
]

SERVICE_MAC_ADDRESS = ""

PASSWD_MYSQL_FOO = ""
PASSWD_MYSQL_BAR = ""
PASSWD_MYSQL_REPLICATION = ""
PASSWD_MYSQL_SLAVE_BACKUP = ""
PASSWD_MYSQL_T04_GITEA = ""

RAID_FS_UUID = ""

SAMBA_PERSONAL_SHARE_MOUNT_OPTIONS = ""

PSA_IP = ""
