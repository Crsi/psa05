#!/usr/bin/env python3

import urllib3
import requests
from icmplib import ping

from common import BaseTest, main
from config import PASSWD_MYSQL_T04_GITEA, PSA_IP


class Tests06(BaseTest):
    def test_gitea_server_running(self):
        for line in self.execute("service --status-all").stdout.decode("UTF-8").split("\n"):
            if "gitea" in line:
                self.assertIn("+", line)
        self.systemd("gitea.service")

    def test_gitea_db_host_ping(self):
        host = ping("192.168.4.2", count=2, interval=0.3, timeout=0.05)
        self.assertTrue(host.is_alive, "Team 04 database server not reachable")

    def test_gitea_db_user_login(self):
        stdout = self.execute(
            f"mysql -u psa_team05 -p{PASSWD_MYSQL_T04_GITEA} -h 192.168.4.2 -e 'SHOW DATABASES;'"
        ).stdout.decode("UTF-8")
        self.assertIn("information_schema", stdout)
        self.assertIn("team05_gitea", stdout)
        self.execute(
            f"mysql -u psa_team05 -p{PASSWD_MYSQL_T04_GITEA} -h 192.168.4.2 -e 'SHOW TABLES;' team05_gitea"
        )

    def test_gitea_reachable(self):
        urllib3.disable_warnings()
        self.assertEqual(requests.get("https://localhost:3200", verify=False).status_code, 200)
        self.assertEqual(requests.get("https://127.0.0.1:3200", verify=False).status_code, 200)
        self.assertEqual(requests.get("https://192.168.5.3:3200", verify=False).status_code, 200)
        self.assertEqual(requests.get("https://192.168.5.4:3200", verify=False).status_code, 200)

    def test_gitea_reachable_globally(self):
        urllib3.disable_warnings()
        try:
            self.assertEqual(requests.get(f"https://{PSA_IP}:60542", verify=False, timeout=3).status_code, 200)
        except requests.exceptions.ConnectionError as exc:
            raise self.failureException(
                f"Couldn't reach Gitea via global port 60542. Make sure general "
                f"internet connectivity is stable. Details: {str(exc)}"
            ) from None


if __name__ == "__main__":
    main()
