#!/usr/bin/python3
import os
teams = [
("ge52dah", "ge36mov"),
("ga48luc", "ga27ruz"),
("ga84qow", "ga84niz"),
("ge25tek", "ge97leb", "ge93tuc"),
("ge96raf", "ga92xak"),
("ge37sir", "ge75civ"),
("ge36gif", "ga65wal"),
("ga83vuf", "ge36kuj"),
("ge75pec", "ge49vaz")]

for i, team in enumerate(teams):
  i += 1
  for j, member in enumerate(team):
    j += 1

    ssh3_cmd = f"chown {1000+j+10*i}:{1000+10*i} -R /home/{member}/.cgi-bin"
    print(ssh3_cmd)
    os.system(ssh3_cmd)

    ssh4_cmd = f"chmod -R +x /home/{member}/.cgi-bin/"
    print(ssh4_cmd)
    os.system(ssh4_cmd)
