
# Create User & Groups
groupadd -g 1010 team01
groupadd -g 1020 team02
groupadd -g 1030 team03
groupadd -g 1040 team04
groupadd -g 1050 team05
groupadd -g 1060 team06
groupadd -g 1070 team07
groupadd -g 1080 team08
groupadd -g 1090 team09

python3 creatuser.py

# Load needed Apache Mods
a2enmod macro
a2enmod ssl
a2enmod suexec
a2enmod cgi
a2enmod userdir
a2enmod rewrite

#Set SeverName
echo "ServerName localhost" >> "/etc/apache2/apache2.conf" #To configure

# Create Dummy Website
echo "<h1>Hallo</h1>" >> "/home/ga92xak/.html-data/index.html"
echo "#!/usr/bin/python3" >> "/home/ga92xak/.cgi-bin/test.cgi"
echo "import getpass" >> "/home/ga92xak/.cgi-bin/test.cgi"
echo "print( \"Content-type: text/html\" )" >> "/home/ga92xak/.cgi-bin/test.cgi"
echo "print( \"\n\n\" )" >> "/home/ga92xak/.cgi-bin/test.cgi"
echo "print( \"Hallo ich bin \" + getpass.getuser())" >> "/home/ga92xak/.cgi-bin/test.cgi"
# Set File Permissions
python3 setupChown.py

# SSL/TLS Configuration
echo "" > "/etc/apache2/sites-available/default-ssl.conf"
echo "<IfModule mod_ssl.c>" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "  <VirtualHost *:443>" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    ServerAdmin sailerq@in.tum.de" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    ServerName localhost" >> "/etc/apache2/sites-available/default-ssl.conf" # To configure
echo "    DocumentRoot /var/www/html" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    ErrorLog \${APACHE_LOG_DIR}/error.log" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    CustomLog \${APACHE_LOG_DIR}/access.log combined" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    SSLEngine on" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    SSLCertificateFile    /etc/apache2/ssl/apache.crt" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    SSLCertificateKeyFile /etc/apache2/ssl/apache.key" >> "/etc/apache2/sites-available/default-ssl.conf"
# Set Rules for CGI
echo "    RewriteEngine On " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    RewriteRule \"^/~([^/]+)(?!/cgi-bin/)/(.*)\" \"/home/\$1/.html-data/\$2\" " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    RewriteRule \"^/~([^/]+)/cgi-bin/(.*)\" \"/~\$1/\$2\" [PT] " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    RewriteRule \"^/~([^/]+)(?!cgi-bin/)\" \"/home/$1/.html-data/index.html\" " >> "/etc/apache2/sites-available/default-ssl.conf"
echo " " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    UserDir disabled root " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    UserDir .cgi-bin " >> "/etc/apache2/sites-available/default-ssl.conf"
echo " " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    <Directory /home/*/.html-data> " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "        Require all granted " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    </Directory> " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "     " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    <Directory /home/*/.cgi-bin> " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "        Require all granted " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "        Options ExecCGI " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "        SetHandler cgi-script " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "    </Directory> " >> "/etc/apache2/sites-available/default-ssl.conf"
echo "   </VirtualHost>" >> "/etc/apache2/sites-available/default-ssl.conf"
echo "</IfModule>" >> "/etc/apache2/sites-available/default-ssl.conf"



#Configure Port 80 Virtual Host
echo "" > "/etc/apache2/sites-available/000-default.conf"
echo "<IfModule mod_ssl.c>" >> "/etc/apache2/sites-available/000-default.conf"
echo "  <VirtualHost *:80>" >> "/etc/apache2/sites-available/000-default.conf"
echo "    ServerAdmin sailerq@in.tum.de" >> "/etc/apache2/sites-available/000-default.conf"
echo "    ServerName localhost" >> "/etc/apache2/sites-available/000-default.conf" # To configure
echo "    DocumentRoot /var/www/html" >> "/etc/apache2/sites-available/000-default.conf"
echo "    ErrorLog \${APACHE_LOG_DIR}/error.log" >> "/etc/apache2/sites-available/000-default.conf"
echo "    CustomLog \${APACHE_LOG_DIR}/access.log combined" >> "/etc/apache2/sites-available/000-default.conf"
# Set Rules for CGI
echo "    RewriteEngine On " >> "/etc/apache2/sites-available/000-default.conf"
echo "    RewriteRule \"^/~([^/]+)(?!/cgi-bin/)/(.*)\" \"/home/\$1/.html-data/\$2\" " >> "/etc/apache2/sites-available/000-default.conf"
echo "    RewriteRule \"^/~([^/]+)/cgi-bin/(.*)\" \"/~\$1/\$2\" [PT] " >> "/etc/apache2/sites-available/000-default.conf"
echo "    RewriteRule \"^/~([^/]+)(?!cgi-bin/)\" \"/home/$1/.html-data/index.html\" " >> "/etc/apache2/sites-available/000-default.conf"
echo " " >> "/etc/apache2/sites-available/000-default.conf"
echo "    UserDir disabled root " >> "/etc/apache2/sites-available/000-default.conf"
echo "    UserDir .cgi-bin " >> "/etc/apache2/sites-available/000-default.conf"
echo " " >> "/etc/apache2/sites-available/000-default.conf"
echo "    <Directory /home/*/.html-data> " >> "/etc/apache2/sites-available/000-default.conf"
echo "        Require all granted " >> "/etc/apache2/sites-available/000-default.conf"
echo "    </Directory> " >> "/etc/apache2/sites-available/000-default.conf"
echo "     " >> "/etc/apache2/sites-available/000-default.conf"
echo "    <Directory /home/*/.cgi-bin> " >> "/etc/apache2/sites-available/000-default.conf"
echo "        Require all granted " >> "/etc/apache2/sites-available/000-default.conf"
echo "        Options ExecCGI " >> "/etc/apache2/sites-available/000-default.conf"
echo "        SetHandler cgi-script " >> "/etc/apache2/sites-available/000-default.conf"
echo "    </Directory> " >> "/etc/apache2/sites-available/000-default.conf"
echo "   </VirtualHost>" >> "/etc/apache2/sites-available/000-default.conf"
echo "</IfModule>" >> "/etc/apache2/sites-available/000-default.conf"





a2ensite default-ssl.conf
service apache2 restart

# CGI
# Enable Suexec
echo "Suexec On " >> "/etc/apache2/apache2.conf"
# Configure Suexec User Dir
echo "/var/www" > "/etc/apache2/suexec/www-data"
echo ".cgi-bin" >> "/etc/apache2/suexec/www-data"

service apache2 restart
#Logging
#Remove IP-addresses from normal Log
sed -i -- 's/%h//g' /etc/apache2/apache2.conf

#Configure Logrotation for normal Log
echo "" > "/etc/logrotate.d/apache2"
echo "/var/log/apache2/access.log {" >> "/etc/logrotate.d/apache2"
echo "        daily" >> "/etc/logrotate.d/apache2"
echo "        missingok" >> "/etc/logrotate.d/apache2"
echo "        rotate 5 " >> "/etc/logrotate.d/apache2"
echo "        compress" >> "/etc/logrotate.d/apache2"
echo "        delaycompress" >> "/etc/logrotate.d/apache2"
echo "        notifempty" >> "/etc/logrotate.d/apache2"
echo "        create 640 root adm" >> "/etc/logrotate.d/apache2"
echo "        sharedscripts" >> "/etc/logrotate.d/apache2"
echo "        postrotate" >> "/etc/logrotate.d/apache2"
echo "                if invoke-rc.d apache2 status > /dev/null 2>&1; then \ " >> "/etc/logrotate.d/apache2"
echo "                    invoke-rc.d apache2 reload > /dev/null 2>&1; \ " >> "/etc/logrotate.d/apache2"
echo "                fi;" >> "/etc/logrotate.d/apache2"
echo "        endscript" >> "/etc/logrotate.d/apache2"
echo "        prerotate" >> "/etc/logrotate.d/apache2"
echo "                if [ -d /etc/logrotate.d/httpd-prerotate ]; then \ " >> "/etc/logrotate.d/apache2"
echo "                        run-parts /etc/logrotate.d/httpd-prerotate; \ " >> "/etc/logrotate.d/apache2"
echo "                fi; \ " >> "/etc/logrotate.d/apache2"
echo "        endscript" >> "/etc/logrotate.d/apache2"
echo "}" >> "/etc/logrotate.d/apache2"

# Configure Logrotation for Error Log
echo "" > "/etc/logrotate.d/apache2error"
echo "/var/log/apache2/error.log {" >> "/etc/logrotate.d/apache2error"
echo "        daily" >> "/etc/logrotate.d/apache2"
echo "        missingok" >> "/etc/logrotate.d/apache2"
echo "        rotate 1" >> "/etc/logrotate.d/apache2"
echo "        compress" >> "/etc/logrotate.d/apache2"
echo "        delaycompress" >> "/etc/logrotate.d/apache2"
echo "        notifempty" >> "/etc/logrotate.d/apache2"
echo "        create 640 root adm" >> "/etc/logrotate.d/apache2"
echo "        sharedscripts" >> "/etc/logrotate.d/apache2"
echo "        postrotate" >> "/etc/logrotate.d/apache2"
echo "                if invoke-rc.d apache2 status > /dev/null 2>&1; then \ " >> "/etc/logrotate.d/apache2"
echo "                    invoke-rc.d apache2 reload > /dev/null 2>&1; \ " >> "/etc/logrotate.d/apache2"
echo "                fi;" >> "/etc/logrotate.d/apache2"
echo "        endscript" >> "/etc/logrotate.d/apache2"
echo "        prerotate" >> "/etc/logrotate.d/apache2"
echo "                if [ -d /etc/logrotate.d/httpd-prerotate ]; then \ " >> "/etc/logrotate.d/apache2"
echo "                        run-parts /etc/logrotate.d/httpd-prerotate; \ " >> "/etc/logrotate.d/apache2"
echo "                fi; \ " >> "/etc/logrotate.d/apache2"
echo "        endscript" >> "/etc/logrotate.d/apache2"
echo "}" >> "/etc/logrotate.d/apache2"


