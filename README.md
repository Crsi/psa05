# Collection of PSA scripts and config files

This repository is a collection of small helper scripts and config
files used during the practical course system administration 2021/22.
Some scripts require to be run in the correct environment,
e.g. to find the relevant IP addresses and hostnames.
