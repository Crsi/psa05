#!/usr/bin/env python3

import os
import csv
import base64
import datetime

from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa


BASE_DN = "ou=Users,dc=team05,dc=psa,dc=in,dc=tum,dc=de"
COLUMN_LIST = [
    "surname", "givenName", "gender", "dateOfBirth", "placeOfBirth", "citizenship",
    "street", "postalCode", "postalCity", "telephoneNumber", "employeeNumber"
]
CSV_USERS_PATH = "sample_users.csv"
LDIF_TEMPLATE_PATH = "psa-user.ldif.template"
MIN_UID = 2000
MIN_GID = 2000

with open(LDIF_TEMPLATE_PATH) as _f:
    LDIF_TEMPLATE = _f.read()


def map_date(date: str) -> str:
    d, m, y = date.split(".")
    if len(y) == 2:
        y = "19" + y
    return f"{y}{m}{d}000000Z"


def read_user_entries():
    users = []
    with open(CSV_USERS_PATH) as f:
        reader = csv.reader(f)
        first_line = None
        uid_number = MIN_UID
        for row in reader:
            if first_line is None:
                first_line = row
                continue
            entry = dict(zip(COLUMN_LIST, row))
            uid_number += 1
            entry.update(dateOfBirth=map_date(entry["dateOfBirth"]))
            entry["uid"] = entry["employeeNumber"]
            entry["uidNumber"] = uid_number
            entry["gidNumber"] = MIN_GID
            entry["homeDirectory"] = f"/mnt/external_homes/{uid_number}"
            entry["baseDN"] = BASE_DN
            entry["cn"] = f'{entry["givenName"]} {entry["surname"]}'
            users.append(entry)
    return users


def make_directories(entries) -> None:
    for entry in entries:
        if not os.path.exists(entry["homeDirectory"]):
            os.mkdir(entry["homeDirectory"])
        os.chmod(entry["homeDirectory"], 0o750)
        os.chown(entry["homeDirectory"], uid=entry["uidNumber"], gid=entry["gidNumber"])


def gen_keys(entries):
    for entry in entries:
        key_path = os.path.join(entry["homeDirectory"], "private_x509_key.pem")
        cert_path = os.path.join(entry["homeDirectory"], "public_x509.pem")
        if os.path.exists(key_path) and os.path.exists(cert_path):
            with open(cert_path, "rb") as f:
                cert = x509.load_pem_x509_certificate(f.read())
            encoded_cert = base64.b64encode(cert.public_bytes(serialization.Encoding.DER))
            entry["userCertificateBinary"] = encoded_cert.decode("ASCII")
            continue

        key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
        with open(key_path, "wb") as f:
            f.write(key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            ))
        os.chmod(key_path, 0o600)
        os.chown(key_path, uid=entry["uidNumber"], gid=entry["gidNumber"])

        subject = issuer = x509.Name([
            x509.NameAttribute(NameOID.COUNTRY_NAME, "DE"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, "Bavaria"),
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, "PSA"),
            x509.NameAttribute(NameOID.COMMON_NAME, entry["cn"]),
            x509.NameAttribute(NameOID.SURNAME, entry["surname"]),
            x509.NameAttribute(NameOID.GIVEN_NAME, entry["givenName"]),
        ])

        cert = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            key.public_key()
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=356)
        ).sign(key, hashes.SHA256())
        with open(cert_path, "wb") as f:
            f.write(cert.public_bytes(serialization.Encoding.PEM))
        os.chmod(cert_path, 0o644)
        os.chown(cert_path, uid=entry["uidNumber"], gid=entry["gidNumber"])
        encoded_cert = base64.b64encode(cert.public_bytes(serialization.Encoding.DER))
        entry["userCertificateBinary"] = encoded_cert.decode("ASCII")


def main():
    users = read_user_entries()
    make_directories(users)
    gen_keys(users)
    print("\n\n".join([LDIF_TEMPLATE.format(**user) for user in users]))


if __name__ == "__main__":
    main()
