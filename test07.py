#!/usr/bin/env python3

import os

from common import BaseTest, main
from config import RAID_FS_UUID, SAMBA_PERSONAL_SHARE_MOUNT_OPTIONS


class Tests07(BaseTest):
    def test_loaded_raid_kernel_module(self):
        self.assertGreaterEqual(
            len([
                line for line in self.execute("lsmod").stdout.decode("UTF-8").split("\n")
                if "raid456" in line
            ]),
            5
        )

    def test_using_proc_md_stat(self):
        self.assertTrue(os.path.exists("/proc/mdstat"))
        with open("/proc/mdstat") as f:
            content = f.read()
        self.assertIn("active raid6", content)
        self.assertIn("unused devices: <none>", content)
        self.assertIn("[UUUUUUUU]", content, "some disks probably not up!")

    def test_raid_has_min_10_gigabytes_capacity(self):
        device_path = os.path.realpath(f"/dev/disk/by-uuid/{RAID_FS_UUID}").split("/")[-1]
        self.assertTrue(device_path.startswith("md"), "wrong device?")
        with open(f"/sys/block/{device_path}/size") as f:
            size = int(f.read()) * 512
        self.assertGreaterEqual(size, 10 * 1024 ** 3)

    def test_nfs_server_running(self):
        self.systemd("nfs-server.service")
        self.systemd("nfs-kernel-server.service")
        self.nmap("192.168.5.4", [(2049, "tcp", "open")])

    def test_mount_nfs_share(self):
        tmp_dir = "/tmp/" + self.random_string(16)
        os.mkdir(tmp_dir)
        try:
            self.execute(f"mount 192.168.5.4:/srv/nfs/home/ge96raf {tmp_dir}")
            self.assertTrue(os.path.exists(f"{tmp_dir}"))
            self.assertTrue(os.path.exists(f"{tmp_dir}/.profile"))
            self.assertTrue(os.path.exists(f"{tmp_dir}/.ssh/authorized_keys"))
            with self.assertRaises(OSError):
                test_file = os.path.join(tmp_dir, "test_file")
                with open(test_file, "w") as f:
                    f.write("test")
                if os.path.exists(test_file):
                    os.unlink(test_file)
        except:
            raise
        finally:
            self.execute(f"umount {tmp_dir}")
            os.rmdir(tmp_dir)

    def test_mount_nfs_share_remotely(self):
        tmp_dir = "/tmp/" + self.random_string(16)
        p = self.execute(
            f'ssh 192.168.5.1 -- "mkdir {tmp_dir} '
            f'&& mount 192.168.5.4:/srv/nfs/home/ge96raf {tmp_dir} || exit 65 '
            f'&& ls -lna {tmp_dir} '
            f'&& findmnt {tmp_dir} '
            f'&& umount {tmp_dir} || exit 67 '
            f'&& rmdir {tmp_dir} || exit 69"',
            code=None
        )
        if p.returncode == 65:
            self.fail(f"Failed to mount NFS share remotely (target {tmp_dir})!")
        elif p.returncode == 67:
            self.fail(f"Failed to unmount remote NFS share from {tmp_dir}!")
        elif p.returncode == 69:
            self.fail(f"Failed to remove directory {tmp_dir} from remote side!")
        elif p.returncode != 0:
            self.fail("Something went wrong checking the remote NFS share capabilities!")

        stdout = p.stdout.decode("UTF-8")
        self.assertIn("addr=192.168.5.4", stdout, "missing mount options")
        self.assertIn("1051 1050  4096", stdout, "failed user mapping")

    def test_smb_server_running(self):
        self.systemd("smbd.service")
        self.nmap("192.168.5.4", [(111, "tcp", "open"), (139, "tcp", "open"), (445, "tcp", "open")])

    def test_smb_clients_exist(self):
        stdout = self.execute("pdbedit -L").stdout.decode("UTF-8")
        self.assertIn("root", stdout)
        self.assertIn("ga92xak", stdout)
        self.assertIn("ge96raf", stdout)

    def test_mount_smb_personal_share(self):
        tmp_dir = "/tmp/" + self.random_string(16)
        os.mkdir(tmp_dir)
        try:
            self.execute(f"mount -t cifs //192.168.5.4/home_ge96raf {tmp_dir} -o {SAMBA_PERSONAL_SHARE_MOUNT_OPTIONS}")
            self.assertTrue(os.path.exists(f"{tmp_dir}"))
            self.assertTrue(os.path.exists(f"{tmp_dir}/.profile"))
            self.assertTrue(os.path.exists(f"{tmp_dir}/.ssh/authorized_keys"))
            test_file = os.path.join(tmp_dir, "test_file")
            with open(test_file, "w") as f:
                f.write("test")
            if os.path.exists(test_file):
                os.unlink(test_file)
        except:
            raise
        finally:
            self.execute(f"umount {tmp_dir}")
            os.rmdir(tmp_dir)

    def test_mount_smb_personal_share_remotely(self):
        tmp_dir = "/tmp/" + self.random_string(16)
        tmp_file = os.path.join(tmp_dir, self.random_string(16))
        p = self.execute(
            f'ssh 192.168.5.1 -- "mkdir {tmp_dir} '
            f'&& mount -t cifs //192.168.5.4/home_ge96raf {tmp_dir} -o {SAMBA_PERSONAL_SHARE_MOUNT_OPTIONS} || exit 71 '
            f'&& ls -lna {tmp_dir} '
            f'&& findmnt {tmp_dir} '
            f'&& echo 1 > {tmp_file} || echo WRITE_FAILED '
            f'&& rm -vf {tmp_file} '
            f'&& umount {tmp_dir} || exit 73 '
            f'&& rmdir {tmp_dir} || exit 75"',
            code=None
        )
        if p.returncode == 71:
            self.fail(f"Failed to mount SMB share remotely (target {tmp_dir})!")
        elif p.returncode == 73:
            self.fail(f"Failed to unmount remote SMB share from {tmp_dir}!")
        elif p.returncode == 75:
            self.fail(f"Failed to remove directory {tmp_dir} from remote side!")
        elif p.returncode != 0:
            self.fail("Something went wrong checking the remote NFS share capabilities!")

        stdout = p.stdout.decode("UTF-8")
        self.assertNotIn("WRITE_FAILED", stdout)
        self.assertIn("addr=192.168.5.4", stdout, "missing mount options")
        self.assertIn("1051 1050     0", stdout, "failed user mapping")
        self.assertIn(f"removed '{tmp_file}'", stdout)

    def test_autofs_service(self):
        self.systemd("autofs.service")

    def test_automount_random_dir(self):
        current_files = os.listdir("/home")
        random_file = os.path.join("/home", self.random_string(16), self.random_string(8))
        with self.assertRaises(OSError) as exc:
            with open(random_file, "w") as f:
                f.write("test")
        self.assertEqual(exc.exception.errno, 30, "no read-only file system?")
        self.assertFalse(os.path.exists(random_file))
        self.assertTrue(os.path.exists(os.path.join("/home", self.random_string(16))))
        self.assertTrue(os.path.exists(os.path.join("/home", self.random_string(16), "YOUR_HOME_WAS_NOT_MOUNTED")))
        with self.assertRaises(PermissionError):
            open("/home/" + self.random_string(16), "w")
        self.assertGreaterEqual(len(os.listdir("/home")), len(current_files) + 3)


if __name__ == "__main__":
    main()
