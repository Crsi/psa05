#!/usr/bin/env python3

import os
import time
import secrets
from timeit import default_timer

from dhcppython import packet
from dhcppython.client import DHCPClient, Lease

from common import BaseTest, main
from config import OUR_DOMAIN, SERVICE_MAC_ADDRESS


TEAM_04_DNS = "192.168.4.3"
TEAM_06_DNS = "192.168.6.3"

TEAM_04_DOMAIN = "psa-team04.in.tum.de"
TEAM_06_DOMAIN = "psa-team06.in.tum.de"


def get_lease(mac_address: str, dhcp_server: str) -> Lease:
    client = DHCPClient()

    # Discover
    discover = packet.DHCPPacket.Discover(mac_address, use_broadcast=True, option_list=None, relay=None)
    tx_id = discover.xid
    start = default_timer()
    client.send_discover(dhcp_server, discover, 0)

    # Offer
    tries = 0
    while not (offer := client.receive_offer(tx_id, 0)):
        time.sleep(client.retry_interval / 1000)
        client.send_discover(dhcp_server, discover, 0)
        if tries > client.max_tries:
            raise RuntimeError(f"Unable to obtain an offer from DHCP server {dhcp_server}")
        tries += 1

    # Request
    request = packet.DHCPPacket.Request(mac_address, int(default_timer() - start), tx_id, client_ip=offer.yiaddr)
    client.send_request(dhcp_server, request, 0)

    # Acknowledge
    tries = 0
    while not (ack := client.receive_ack(tx_id, 0)):
        time.sleep(client.retry_interval / 1000)
        client.send_request(dhcp_server, request, 0)
        if tries > client.max_tries:
            raise RuntimeError(f"Unable to obtain an acknowledgement from DHCP server {dhcp_server}")
        tries += 1

    lease_time = default_timer() - start
    for socket in client.writing_sockets:
        socket.close()
    for socket in client.listening_sockets:
        socket.close()

    return Lease(discover, offer, request, ack, lease_time, client.ack_server)


class Tests03(BaseTest):
    def test_named_running(self):
        self.systemd("named.service", ip="192.168.5.2")

    def test_resolving_via_host(self):
        for name, ip in [
            ("monitor.psa-team05.in.tum.de", "192.168.5.1"),
            ("gateway.psa-team05.in.tum.de", "192.168.5.2"),
            ("unknown.psa-team05.in.tum.de", "192.168.5.3"),
            ("services.psa-team05.in.tum.de", "192.168.5.4"),
            ("node01.psa-team05.in.tum.de", "192.168.5.1"),
            ("node02.psa-team05.in.tum.de", "192.168.5.2"),
            ("node03.psa-team05.in.tum.de", "192.168.5.3")
        ]:
            self.assertIn(
                f"{name} has address {ip}",
                self.execute(f"host {name}").stdout.decode("UTF-8").strip()
            )

    def test_resolve_gitlab_timed(self):
        start = time.time()
        self.dig("gitlab.com")
        end = time.time()
        self.assertLessEqual(end - start, 2.0, "Resolving DNS takes pretty long")

    def test_resolve_random_name_timed(self):
        name = secrets.token_urlsafe(10).replace("/", "-").replace(".", "-").lower() + ".com"
        start = time.time()
        self.dig(name)
        end = time.time()
        self.assertLessEqual(end - start, 5.0, f"Resolving DNS {name!r} takes pretty long")

    def test_resolve_using_gateway(self):
        def dig_default(name: str, *args):
            return self.dig(name, *args, nameserver="192.168.5.2")

        self.assertIn("A\t192.168.5.1", dig_default("monitor.psa-team05.in.tum.de"))
        self.assertIn("A\t192.168.5.2", dig_default("gateway.psa-team05.in.tum.de"))
        self.assertIn("CNAME\tgateway.psa-team05.in.tum.de", dig_default("firewall.psa-team05.in.tum.de"))
        self.assertIn("CNAME\tgateway.psa-team05.in.tum.de", dig_default("dns.psa-team05.in.tum.de"))
        self.assertIn("A\t192.168.5.3", dig_default("unknown.psa-team05.in.tum.de"))
        self.assertIn("A\t192.168.5.4", dig_default("services.psa-team05.in.tum.de"))
        self.assertIn("CNAME\tservices.psa-team05.in.tum.de", dig_default("gitea.psa-team05.in.tum.de"))

        self.assertIn("MX\t8 mail.psa-team05.in.tum.de", dig_default("psa-team05.in.tum.de", "MX"))
        self.assertIn("SOA\tgateway.psa-team05.in.tum.de", dig_default("psa-team05.in.tum.de", "SOA"))

        self.assertIn("PTR\tmonitor.psa-team05.in.tum.de", dig_default("192.168.5.1", "-x"))
        self.assertIn("PTR\tgateway.psa-team05.in.tum.de", dig_default("192.168.5.2", "-x"))
        self.assertIn("PTR\tmail.psa-team05.in.tum.de", dig_default("192.168.5.3", "-x"))
        self.assertIn("PTR\tldap.psa-team05.in.tum.de", dig_default("192.168.5.3", "-x"))
        self.assertIn("PTR\tunknown.psa-team05.in.tum.de", dig_default("192.168.5.3", "-x"))
        self.assertIn("PTR\tservices.psa-team05.in.tum.de", dig_default("192.168.5.4", "-x"))
        self.assertIn("PTR\tgitea.psa-team05.in.tum.de", dig_default("192.168.5.4", "-x"))
        self.assertIn("PTR\tmain.psa-team05.in.tum.de", dig_default("192.168.5.4", "-x"))

        self.assertIn("PTR\tnode01.psa-team05.in.tum.de", dig_default("192.168.5.1", "-x"))
        self.assertIn("PTR\tnode02.psa-team05.in.tum.de", dig_default("192.168.5.2", "-x"))
        self.assertIn("PTR\tnode03.psa-team05.in.tum.de", dig_default("192.168.5.3", "-x"))

        self.assertIn("A\t192.168.5.2", self.dig("gateway.psa-team05.in.tum.de"), "wrong default DNS server")

    def test_gw_resolves_as_slave_for_04(self):
        self.assertIn("SOA", self.dig(TEAM_04_DOMAIN, options="-t SOA", nameserver="192.168.5.2"))

        self.assertIn(f"A\t{TEAM_04_DNS}", self.dig(TEAM_04_DOMAIN, nameserver="192.168.5.2"))
        self.assertIn("A\t192.168.4.1", self.dig("webserver.psa-team04.in.tum.de", nameserver="192.168.5.2"))

        self.assertIn("PTR\trouter.psa-team04.in.tum.de", self.dig(TEAM_04_DNS, options="-x"))
        self.assertIn("PTR\twebserver.psa-team04.in.tum.de", self.dig("192.168.4.1", options="-x"))

    def test_gw_resolves_as_slave_for_06(self):
        self.assertIn("SOA", self.dig(TEAM_06_DOMAIN, options="-t SOA", nameserver="192.168.5.2"))

        self.assertIn(f"A\t{TEAM_06_DNS}", self.dig(TEAM_06_DOMAIN, nameserver="192.168.5.2"))

        self.assertIn("PTR", self.dig(TEAM_06_DNS, options="-x", nameserver="192.168.5.2"))
        self.assertIn("PTR", self.dig("192.168.6.1", options="-x", nameserver="192.168.5.2"))

    def test_team_04_resolves_us_as_slave(self):
        self.assertIn("SOA\tgateway.psa-team05", self.dig(OUR_DOMAIN, options="-t SOA", nameserver=TEAM_04_DNS))

        self.assertIn("A\t192.168.5.2", self.dig("gateway.psa-team05.in.tum.de", nameserver=TEAM_04_DNS))
        self.assertIn("A\t192.168.5.1", self.dig("node01.psa-team05.in.tum.de", nameserver=TEAM_04_DNS))
        self.assertIn("SOA\tgateway.psa-team05.in.", self.dig("services.psa-team05.in.tum.de", nameserver=TEAM_04_DNS))

    def test_team_06_resolves_us_as_slave(self):
        self.assertIn("SOA\tgateway.psa-team05", self.dig(OUR_DOMAIN, options="-t SOA", nameserver=TEAM_06_DNS))

        self.assertIn("A\t192.168.5.2", self.dig("gateway.psa-team05.in.tum.de", nameserver=TEAM_06_DNS))
        self.assertIn("A\t192.168.5.1", self.dig("monitor.psa-team05.in.tum.de", nameserver=TEAM_06_DNS))

    def test_dhcp_server_running(self):
        self.systemd("isc-dhcp-server.service", ip="192.168.5.2")

    def test_dhcp_full_dora_cycle(self):
        self.assertEqual(os.geteuid(), 0, "This check requires root privileges!")
        lease = get_lease(SERVICE_MAC_ADDRESS, "192.168.5.2")

        self.assertEqual(str(lease.discover.ciaddr), "0.0.0.0")
        self.assertEqual(str(lease.offer.yiaddr), "192.168.5.3")
        self.assertEqual(str(lease.request.ciaddr), "192.168.5.3")
        self.assertEqual(str(lease.request.yiaddr), "0.0.0.0")
        self.assertEqual(str(lease.ack.ciaddr), "192.168.5.3")
        self.assertEqual(str(lease.ack.yiaddr), "192.168.5.3")


if __name__ == "__main__":
    main()
