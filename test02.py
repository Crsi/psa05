#!/usr/bin/env python3

import requests
from icmplib import ping, traceroute

from common import BaseTest, main
from config import PSA_IP, GATEWAY_IPS, REMOTE_ROUTER_IPS, ALL_GROUP_ROUTERS


GATEWAY_IP = "192.168.5.2"


class Tests02(BaseTest):
    def test_ping_gateway(self):
        self.assertTrue(ping("192.168.5.2", count=2, timeout=0.005).is_alive)

    def test_ssh_gateway(self):
        self.assertTrue(self.execute(f"ssh root@192.168.5.2 whoami").stdout.startswith(b"root"))

    def test_nmap_gateway(self):
        self.nmap(GATEWAY_IP, [(22, "tcp", "open")])

    def test_nmap_global_open_port(self):
        self.nmap(PSA_IP, [(60520, "tcp", "open"), (60542, "tcp", "open")])

    def test_ssh_all_tests_script_gw(self):
        self.execute(f"ssh root@{GATEWAY_IP} /root/tests.sh")

    def test_ssh_node04(self):
        self.assertTrue(self.execute("ssh root@192.168.5.4 whoami").stdout.startswith(b"root"))

    def test_ping_own_interface_ips_from_gw(self):
        for node in GATEWAY_IPS:
            self.execute(f"ssh root@{GATEWAY_IP} ping -c 2 {node}")

    def test_ping_other_groups_from_gw(self):
        for node in REMOTE_ROUTER_IPS:
            self.assertTrue(ping(node, count=2, timeout=0.025).is_alive, f"{node} not reachable")

    def test_global_pinging(self):
        for node in ["1.1.1.1", "gitlab.com", "tum.de"]:
            self.assertTrue(ping(node, count=2, timeout=1).is_alive, f"{node} not reachable")

    def test_global_curl(self):
        self.assertTrue(f"curl http://tum.de")
        self.assertTrue(f"curl https://tum.de")

    def test_ping_all_subnet_routers(self):
        failed_groups = ", ".join([
            ip for ip in [f"192.168.{group[0]}.{group[1]}" for group in ALL_GROUP_ROUTERS]
            if not ping(ip, count=2, timeout=0.1).is_alive
        ])
        if failed_groups:
            self.fail(f"Unable to ping group routers: {failed_groups}")

    def test_fw_is_full_gw(self):
        result = traceroute("1.1.1.1", timeout=0.1, max_hops=16)
        self.assertTrue(result[0].is_alive)
        self.assertEqual(result[0].distance, 1)
        self.assertEqual(result[0].address, "192.168.5.2", "first gateway is not ours")
        self.assertEqual(result[-1].address, "1.1.1.1")

    def test_ping_all_groups_some_hosts(self):
        failed_hosts = ", ".join([
            f"192.168.{ip_third}.{ip_fourth}" for ip_third, ip_fourth in [
                (1, 1), (2, 4), (3, 2), (4, 1), (5, 1), (5, 2), (5, 3), (5, 4), (6, 4), (7, 3), (8, 2), (9, 4)
            ]
            if not ping(f"192.168.{ip_third}.{ip_fourth}", count=2, timeout=0.1).is_alive
        ])
        if failed_hosts:
            self.fail(f"Unable to ping certain hosts of other groups: {failed_hosts}")

    def test_iptables_monitor_active(self):
        stdout = self.execute("ssh 192.168.5.1 iptables -L -n").stdout.decode("UTF-8")
        self.assertGreater(stdout.count("\n"), 32)

    def test_iptables_firewall_nat(self):
        stdout = self.execute("ssh 192.168.5.2 iptables -L -n -t nat").stdout.decode("UTF-8")
        self.assertGreater(stdout.count("\n"), 12)
        for line in stdout.split("\n"):
            if line.split() == "DNAT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp dpt:60542 to:192.168.5.3:3200".split():
                break
        else:
            self.fail("Expected DNAT rule for Gitea in firewall iptables NAT chain")
        for line in stdout.split("\n"):
            if line.split() == "MASQUERADE all -- 192.168.5.0/24 0.0.0.0/0".split():
                break
        else:
            self.fail("Expected MASQUERADE rule for team05 subnet in firewall iptables NAT chain")
        stdout = self.execute("ssh 192.168.5.2 iptables -L -n").stdout.decode("UTF-8")
        self.assertGreater(stdout.count("\n"), 32)

    def test_iptables_app_server_blocks_tcp(self):
        try:
            requests.get("http://45.136.29.221:80", timeout=2)
        except requests.exceptions.ConnectionError:
            pass
        else:
            self.fail("It looks like some outgoing TCP connection was possible!")

    def test_iptables_app_server_notrack(self):
        stdout = self.execute("iptables -L -n -t raw").stdout.decode("UTF-8")
        self.assertGreater(stdout.count("\n"), 4)
        for rule in [
            "CT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp dpt:80 NOTRACK",
            "CT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp dpt:443 NOTRACK",
            "CT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp spt:80 NOTRACK",
            "CT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp spt:443 NOTRACK"
        ]:
            for line in stdout.split("\n"):
                if line.split() == rule.split():
                    break
            else:
                self.fail(f"Expected rule {rule!r} in app server iptables RAW chain")

    def test_iptables_app_server_exceptions(self):
        stdout = self.execute("iptables -L -n").stdout.decode("UTF-8")
        self.assertGreater(stdout.count("\n"), 32)
        for rule in [
            "ACCEPT tcp -- 0.0.0.0/0 172.65.251.78 tcp dpt:80",
            "ACCEPT tcp -- 0.0.0.0/0 172.65.251.78 tcp dpt:443",
            "ACCEPT tcp -- 0.0.0.0/0 129.187.10.100 tcp dpt:80",
            "ACCEPT tcp -- 0.0.0.0/0 129.187.10.100 tcp dpt:443"
        ]:
            for line in stdout.split("\n"):
                if line.split() == rule.split():
                    break
            else:
                self.fail(f"Expected rule {rule!r} in app server iptables FILTER chain")


if __name__ == "__main__":
    main()
