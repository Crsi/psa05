#!/usr/bin/env python3

import shlex
import subprocess


def anon_search():
    okay = True
    details = "Search completed, results okay."

    try:
        p = subprocess.run(
            shlex.split(
                'ldapsearch -H ldaps://ldap.psa-team05.in.tum.de -v -x -b dc=team05,dc=psa,dc=in,dc=tum,dc=de "(cn=*)"'
            ),
            capture_output=True,
            timeout=1
        )
        stdout = p.stdout.decode("UTF-8")

        if "dn: uid=1258111154,ou=Users,dc=team05,dc=psa,dc=in,dc=tum,dc=de\n" not in stdout:
            okay = False
            details = "Check UID 1 not found in search results."
        if "dn: uid=ge96raf,ou=Members,dc=team05,dc=psa,dc=in,dc=tum,dc=de\n" not in stdout:
            okay = False
            details = "Check UID 2 not found in search results."

        for line in stdout.split("\n"):
            if "numEntries" in line:
                entries = int(line.strip().split(" ")[-1])
                if entries < 100:
                    okay = False
                    details = "Not enough search results."

        for forbidden_attr in [
            "entryUUID",
            "createTimestamp",
            "uidNumber",
            "gidNumber",
            "employeeNumber",
            "loginShell",
            "homeDirectory",
            "publicKey",
            "userCertificate",
            "placeOfBirth",
            "dateOfBirth",
            "postalCode",
            "street",
        ]:
            if forbidden_attr in stdout:
                okay = False
                details = "Forbidden search results found."

    except subprocess.TimeoutExpired:
        okay = False
        details = "Subprocess timeout."

    code = (2, 0)[okay]
    return okay, f"{code} \"LDAP Anonymous Search\" - {details}"


if __name__ == "__main__":
    result = anon_search()
    print(result[1])
    exit(result[0])
