#!/usr/bin/env python3

import time
from timeit import default_timer

from dhcppython import packet
from dhcppython.client import DHCPClient, Lease


def get_lease(mac_address: str, dhcp_server: str) -> Lease:
    client = DHCPClient()

    # Discover
    discover = packet.DHCPPacket.Discover(mac_address, use_broadcast=True, option_list=None, relay=None)
    tx_id = discover.xid
    start = default_timer()
    client.send_discover(dhcp_server, discover, 0)

    # Offer
    tries = 0
    while not (offer := client.receive_offer(tx_id, 0)):
        time.sleep(client.retry_interval / 1000)
        client.send_discover(dhcp_server, discover, 0)
        if tries > client.max_tries:
            raise RuntimeError(f"Unable to obtain an offer from DHCP server {dhcp_server}")
        tries += 1

    # Request
    request = packet.DHCPPacket.Request(mac_address, int(default_timer() - start), tx_id, client_ip=offer.yiaddr)
    client.send_request(dhcp_server, request, 0)

    # Acknowledge
    tries = 0
    while not (ack := client.receive_ack(tx_id, 0)):
        time.sleep(client.retry_interval / 1000)
        client.send_request(dhcp_server, request, 0)
        if tries > client.max_tries:
            raise RuntimeError(f"Unable to obtain an acknowledgement from DHCP server {dhcp_server}")
        tries += 1

    lease_time = default_timer() - start
    return Lease(discover, offer, request, ack, lease_time, client.ack_server)


def check_dora():
    code = 0
    details = "Successfully received lease from DHCP server."
    lease_time = -1

    try:
        lease = get_lease("be:ef:13:37:03:02", "192.168.5.2")
        lease_time = lease.time

        if str(lease.discover.ciaddr) != "0.0.0.0":
            code = 1
            details += " Unexpected discover client address."
        if str(lease.offer.yiaddr) != "192.168.5.3":
            code = 1
            details += " Unexpected offer your address."
        if str(lease.request.ciaddr) != "192.168.5.3":
            code = 1
            details += " Unexpected request client address."
        if str(lease.request.yiaddr) != "0.0.0.0":
            code = 1
            details += " Unexpected request your address."
        if str(lease.ack.ciaddr) != "192.168.5.3":
            code = 1
            details += " Unexpected acknowledge client address."
        if str(lease.ack.yiaddr) != "192.168.5.3":
            code = 1
            details += " Unexpected acknowledge your address."

    except RuntimeError as exc:
        code = 2
        details = str(exc)

    return code, f"{code} \"DHCP DORA Cycle\" time_ms={1000 * lease_time:.1f} {details}"


if __name__ == "__main__":
    result = check_dora()
    print(result[1])
    exit(result[0])
