#!/usr/bin/env python3

import shlex
import subprocess


def show_tables(db: str):
    okay = True
    details = "Database query completed, results okay."

    try:
        p = subprocess.run(
            shlex.split(f"mysql {db}"),
            capture_output=True,
            input=b"SHOW TABLES;\n",
            timeout=1
        )
        stdout = p.stdout.decode("UTF-8")

        if p.returncode == 0:
            if "Tables_in_foo" not in stdout or "foo_table" not in stdout:
                okay = False
                details = "Not all expected tables found."
        else:
            okay = False
            details = p.stderr.decode("UTF-8")

    except subprocess.TimeoutExpired:
        okay = False
        details = "Subprocess timeout."

    code = (2, 0)[okay]
    return okay, f"{code} \"MariaDB Table Enumeration\" - {details}"


if __name__ == "__main__":
    result = show_tables("foo")
    print(result[1])
    exit(result[0])
