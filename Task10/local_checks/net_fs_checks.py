#!/usr/bin/env python3

import os
import shlex
import random
import string
import subprocess


def mount_and_check(mount: str, mount_opts: str, mount_type: str, read_only: bool) -> tuple:
    code = 0
    details = f"Successfully mounted and used {mount}."
    tmp_dir = "/tmp/" + "".join(random.choice(string.ascii_lowercase) for _ in range(8))

    try:
        os.mkdir(tmp_dir)
        p = subprocess.run(shlex.split(f"mount -t {mount_type} {mount} -o {mount_opts} {tmp_dir}"), timeout=3)

        if p.returncode == 0:
            if not os.path.exists(f"{tmp_dir}/.profile") or not os.path.exists(f"{tmp_dir}/.ssh/authorized_keys"):
                code = 1
                details += " Local expected files not found. Check privileges and mount points."

            test_file = os.path.join(tmp_dir, "test_file")

            if read_only:
                try:
                    open(test_file, "w").close()
                except OSError:
                    pass
                else:
                    code = code or 1
                    details += " Write allowed on read-only mounted share!"
                    os.unlink(test_file)

            else:
                try:
                    open(test_file, "w").close()
                except OSError:
                    code = code or 1
                    details += " Write not allowed on write-enabled share!"
                else:
                    os.unlink(test_file)

        else:
            code = 2
            details = "Mount failed. " + p.stderr.decode("UTF-8")

    except subprocess.TimeoutExpired:
        code = 2
        details = "Subprocess timeout. Network reachable?"

    finally:
        code |= subprocess.run(shlex.split(f"umount {mount}")).returncode
        os.rmdir(tmp_dir)

    return code, f"{code} \"{mount_type.upper()} Share\" - {details}"


if __name__ == "__main__":
    result1 = mount_and_check(
        "192.168.5.4:/srv/nfs/home/ge96raf",
        "ro",
        "nfs",
        True
    )
    print(result1[1])
    result2 = mount_and_check(
        "//192.168.5.3/home_ge96raf",
        "username=ge96raf,password=samba,uid=1051,gid=1050,file_mode=0640,dir_mode=0750",
        "cifs",
        False
    )
    print(result2[1])
    exit(result1[0] | result2[0])
