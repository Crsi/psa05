#!/usr/bin/python
# Location: ~/local/share/check_mk/web/plugins/wato

group = "activechecks"
register_rule(
    group,
    "active_checks:dhcp2",
    Tuple(
        title=_("Check DHCP/DORA cycle"),
        help=_("Check the correct function of a DHCP server by going through "
               "a full DORA (discover, offer, request, acknowledge) cycle."),
        elements=[
            TextAscii(
                title=_("DHCP server's IP address"),
                allow_empty=False,
                help=_("The IPv4 address you want to ask (server)")
            ),
            TextAscii(
                title=_("IP address to request from DHCP server"),
                allow_empty=False,
                help=_("The IPv4 address you want to query (client)")
            ),
            TextAscii(
                title=_("DHCP client's MAC address"),
                allow_empty=False,
                help=_("MAC (media access control) address to be used for querying the server")
            ),
        ]
    ),
    match='all'
)
