#!/usr/bin/env python3

from common import BaseTest, main


class Tests08(BaseTest):
    def test_openldap_server_running(self):
        self.systemd("slapd.service")

    def test_openldap_server_ports(self):
        self.nmap("127.0.0.1", [(389, "tcp", "closed"), (636, "tcp", "open")])
        self.nmap("192.168.5.3", [(389, "tcp", "closed"), (636, "tcp", "open")])

    def test_anonymous_search(self):
        for host in ["ldapi:///", "ldaps://ldap.psa-team05.in.tum.de"]:
            p = self.execute(f"ldapsearch -H {host} -v -x -b dc=team05,dc=psa,dc=in,dc=tum,dc=de \"(cn=*)\"")
            stdout = p.stdout.decode("UTF-8")
            self.assertIn("dn: uid=1258111154,ou=Users,dc=team05,dc=psa,dc=in,dc=tum,dc=de\n", stdout)
            self.assertIn("dn: uid=ge96raf,ou=Members,dc=team05,dc=psa,dc=in,dc=tum,dc=de\n", stdout)
            for line in stdout.split("\n"):
                if "numEntries" in line:
                    entries = int(line.strip().split(" ")[-1])
                    self.assertGreater(entries, 100)
            for forbidden_attr in [
                "entryUUID",
                "createTimestamp",
                "uidNumber",
                "gidNumber",
                "employeeNumber",
                "loginShell",
                "homeDirectory",
                "publicKey",
                "userCertificate",
                "placeOfBirth",
                "dateOfBirth",
                "postalCode",
                "street",
            ]:
                self.assertNotIn(forbidden_attr, stdout)


if __name__ == "__main__":
    main()
