#!/usr/bin/env python3

import requests

from common import BaseTest, main


class Tests10(BaseTest):
    def test_ssh_tests_script_monitor(self):
        self.execute(f"ssh 192.168.5.1 /root/tests.sh")

    def test_apache_running(self):
        self.systemd("apache2.service", ip="192.168.5.1")

    def test_omd_running(self):
        self.systemd("omd.service", ip="192.168.5.1")

    def test_checkmk_page(self):
        self.assertEqual(requests.get("http://192.168.5.1").status_code, 200)
        self.assertEqual(requests.get("http://192.168.5.1/master").status_code, 200)
        self.assertEqual(requests.get("http://192.168.5.1/master/check_mk/dashboard.py?name=main").status_code, 200)


if __name__ == "__main__":
    main()
