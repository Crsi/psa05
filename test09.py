#!/usr/bin/env python3

import os
import time

from common import BaseTest, main


class Tests09(BaseTest):
    def test_postfix_service_running(self):
        self.systemd("postfix.service")

    def test_amavis_service_running(self):
        self.systemd("amavis.service")

    def test_clamav_service_running(self):
        self.systemd("clamav-daemon.service")
        self.systemd("clamav-freshclam.service")

    def test_dovecot_service_running(self):
        self.systemd("dovecot.service")

    def test_send_new_valid_email(self):
        nonce = self.random_string(20)
        self.execute(f"su ge96raf -c \"echo 'Test: {nonce}' | mutt -s 'Test Mail' -- ga92xak@psa-team05.in.tum.de\"")
        for _ in range(3):
            with open("/home/ge96raf/sent") as f:
                if f"Test: {nonce}" in f.read():
                    break
            time.sleep(1)
        else:
            self.fail("Test mail wasn't sent successfully")

        self.assertTrue(os.path.exists("/home/ga92xak/Maildir/new"))
        found = False
        for path, dirs, files in os.walk("/home/ga92xak/Maildir/new"):
            for file in files:
                with open(os.path.join("/home/ga92xak/Maildir/new", file)) as f:
                    if f"\n\nTest: {nonce}" in f.read():
                        found = True
        self.assertTrue(found, "Test mail didn't reach its destination")


if __name__ == "__main__":
    main()
