#!/usr/bin/env python3

import unittest

from common import BaseTest, main
from test01 import Tests01
from test02 import Tests02
from test03 import Tests03
from test04 import Tests04
from test05 import Tests05
from test06 import Tests06
from test07 import Tests07
from test08 import Tests08
from test09 import Tests09
from test10 import Tests10
from test11 import Tests11


if __name__ == "__main__":
    main()
