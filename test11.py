#!/usr/bin/env python3

import os

from common import BaseTest, main


class Tests11(BaseTest):
    def test_kernel_exploit_not_working(self):
        with open("/etc/crontab", "r") as f:
            original_content = f.read()
        original_directory = os.path.abspath(os.getcwd())
        self.assertTrue(original_content[1] != "x")
        os.chdir("/root/tests/")
        self.assertIn(os.path.abspath(os.getcwd()), ["/root/tests", "/root/tests/", "/root/psa05", "/root/psa05/"])
        self.assertEqual(
            self.execute("su ge96raf -c './write_anything'", code=1).stderr.decode().strip(),
            "Usage: ./write_anything TARGETFILE OFFSET DATA",
            "check file permissions of the kernel exploit script"
        )
        self.execute("su ge96raf -c './write_anything /etc/crontab 1 x'")
        try:
            with open("/etc/crontab", "r") as f:
                self.assertNotEqual(f.read()[1], "x")
        except:
            with open("/etc/crontab", "w") as f:
                f.write(original_content)
            raise
        os.chdir(original_directory)


if __name__ == "__main__":
    main()
